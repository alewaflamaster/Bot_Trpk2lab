import config, msg # Импортируем токен из конфига и текст сообщений, дабы не мешалось в основном коде
from aiogram import Bot, Dispatcher, executor, types
import pandas # Для чтения из Excel
import requests # Для запроса в yandex
from urllib.parse import urlencode # для парсинга ссылки в urlencode

bot = Bot(config.token)
dp = Dispatcher(bot)


buttonBack = types.KeyboardButton('Назад') # Кнопка назад, всегда одна :)

# кнопки для меню с настройками

buttonCurrentSettings = types.KeyboardButton('Текущие настройки')
buttonUrlChannel = types.KeyboardButton('Выставить ссылку на канал')
buttonSetSpeed = types.KeyboardButton('Выставить частоту отправки приглашения')
buttonSetLimit = types.KeyboardButton('Выставить лимит на приглашения в день')

# кнопки для меню загрузки таблицы с номерами
buttonNumbers = types.KeyboardButton('Просмотр загруженных телефонов')
buttonAddExcel = types.KeyboardButton('Загрузить из Excel')
buttonAddUrlYa = types.KeyboardButton('Загрузить из яндекс диска')
    
# кнопки для основного меню
buttonStart = types.KeyboardButton('Запустить инвайтера')
buttonUploadNumber = types.KeyboardButton('Загрузка аккаунтов')
buttonSettings = types.KeyboardButton('Настройки')

back_kb = types.ReplyKeyboardMarkup(resize_keyboard=True).add(buttonBack) # панель с кнопкой назад 

start_kb = types.ReplyKeyboardMarkup(resize_keyboard=True).add(buttonStart).add(buttonUploadNumber).add(buttonSettings) # панель в основном меню, 3 кнопки в ряд

upload_kb = types.ReplyKeyboardMarkup(resize_keyboard=True) # панель для загрузки таблицы, три ряда, во втором ряду две кнопки
upload_kb.row(buttonNumbers)
upload_kb.row(buttonAddExcel,buttonAddUrlYa)
upload_kb.row(buttonBack)

settings_kb = types.ReplyKeyboardMarkup(resize_keyboard=True).add(buttonCurrentSettings).add(buttonUrlChannel).add(buttonSetSpeed).add(buttonSetLimit) # панель для настроек, 5 рядов, в каждой по кнопке
settings_kb.row(buttonBack)

@dp.message_handler(commands=["start"]) # сообщение при команде /start 
async def start(message: types.Message):
    await message.answer(msg.startmessage, reply_markup=start_kb)

@dp.message_handler(content_types=['document']) # Если пришел документ
async def takeexcel(message):
    if message.document.mime_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        message.document.file_name = str(message.from_id) + ".xlsx"
        file_info = await bot.get_file(message.document.file_id)
        file_path = file_info.file_path
        await bot.download_file(file_path, "userfiles/" +  message.document.file_name)

        excel = pandas.read_excel('userfiles/' + str(message.from_id) + '.xlsx', header=None)

        with open("userfiles/" + str(message.from_id) + ".txt", "w") as file: # создаем файл с номерами телефонов
            for i in range(len(excel.values)):
                excelline = str(excel.values[i])
                excelline = excelline.replace("[","")
                excelline = excelline.replace("]","")
                file.write(excelline + "\n")
            file.close()
        await message.reply(msg.excelGood, reply_markup=upload_kb)
    else:
        await message.reply(msg.waitExcel, reply_markup=back_kb)


@dp.message_handler() # отслеживаем сообщения
async def echo(message: types.Message):
    global waitanswer, waitUrlChannel, waitLimit, waitSpeed, waitUrlYandex
    print(message)
    if message.text == 'Назад':
        waitUrlYandex = False
        waitanswer = False
        waitLimit = False
        waitSpeed = False
        waitUrlChannel = False
        await message.reply(msg.mainmenu, reply_markup=start_kb)
    if waitanswer == False:
        if message.text == 'Загрузка аккаунтов':
            await message.reply(msg.pressUpload, reply_markup=upload_kb)        
        if message.text == 'Настройки':
            await message.reply(msg.settingsmenu,reply_markup=settings_kb)
        if message.text == 'Текущие настройки':
            try:
                with open("usersettings/" + str(str(message.from_id)) + ".txt", 'r') as file:
                    lines = file.readlines()
                    await message.reply(msg.currentsettings.format(lines[0].partition(": ")[2],lines[1].partition(": ")[2],lines[2].partition(": ")[2]), reply_markup=settings_kb)
                file.close()
            except:
                await message.reply(msg.currentsettings.format("\n"," \n","\n"), reply_markup=settings_kb)
        if message.text == 'Выставить ссылку на канал':
            await message.reply(msg.sendchannelurl, reply_markup=back_kb)
            waitanswer = True
            waitUrlChannel = True
        if message.text == 'Выставить частоту отправки приглашения':
            await message.reply(msg.sendSpeed, reply_markup=back_kb)
            waitanswer = True
            waitSpeed = True
        if message.text == 'Выставить лимит на приглашения в день':
            await message.reply(msg.sendLimit, reply_markup=back_kb)
            waitanswer = True
            waitLimit = True
        if message.text == 'Загрузить из Excel':
            await message.reply(msg.waitExcel, reply_markup=back_kb)
        if message.text == 'Просмотр загруженных телефонов':
            with open('userfiles/' + str(message.from_id) + ".txt", "r") as file:
                lines = file.readlines()
                numbersmsg = ""
                for line in lines:
                    numbersmsg += line
                await message.reply("Номера телефонов которые есть в базе:\n" + numbersmsg, reply_markup=upload_kb)
                
                file.close()
        if message.text == 'Загрузить из яндекс диска':
            await message.reply(msg.waitYaUrl,reply_markup=back_kb)
            waitanswer = True
            waitUrlYandex = True

    elif waitUrlYandex == True: # https://ru.stackoverflow.com/questions/1088300/как-скачивать-файлы-с-яндекс-диска  пример загрузки из яндекса взял отсюда
        public_key = message.text

        final_url = config.base_url + urlencode(dict(public_key=public_key))
        response = requests.get(final_url)
        download_url = response.json()['href']    

        download_response = requests.get(download_url)
        with open ('userfiles/' + str(message.from_id) + ".xlsx", "wb") as file:
            file.write(download_response.content)
        file.close()

        excel = pandas.read_excel('userfiles/' + str(message.from_id) + '.xlsx', header=None)

        with open("userfiles/" + str(message.from_id) + ".txt", "w") as file: # создаем файл с номерами телефонов
            for i in range(len(excel.values)):
                excelline = str(excel.values[i])
                excelline = excelline.replace("[","")
                excelline = excelline.replace("]","")
                file.write(excelline + "\n")
            file.close()

        waitanswer = False
        waitUrlYandex = False
        await message.reply(msg.YaUrlGood, reply_markup=upload_kb)

    elif waitUrlChannel == True: # Отслеживаем нажатие кнопки на изменение настроек и перезаписываем файл (Если его нет то создаем)
        try:
            open("usersettings/" + str(message.from_id) + ".txt", 'r') # пробуем открыть, если файла не существует вылезает exception
        except:
            open("usersettings/" + str(message.from_id) + ".txt", 'w') # создаем файл с именем id_user + .txt, таким образом все файлы будут индивидуальны

        with open("usersettings/" + str(message.from_id) + ".txt", "r+") as file: # Отслеживаем кол-во строк, если 0 то запихиваем нужные строки и сразу обновляем информацию
            lines = file.readlines() 
            if len(lines) == 0:
                file.write("urlchannel: " + message.text + "\n")
                file.write('speed: ' + "" + "\n")
                file.write('limit: ' + "" + "\n")
                lines = file.readlines()
                file.close()
            else: # Если строк больше 0 то сохроняем данные строк и перзаписываем только нужную :)
                lines[0] = "urlchannel: " + message.text + "\n"
                with open("usersettings/" + str(message.from_id) + ".txt", "w") as newfile: 
                    for line in lines:
                        newfile.write(line)        
                newfile.close()
        file.close()      
                            # Обнуляем ожидания ответа, для доступа к командам
        waitanswer = False
        waitUrlChannel = False
        waitSpeed = False
        waitLimit = False
        await message.reply(msg.urlGood, reply_markup=settings_kb)
        
    elif waitSpeed == True: # Описано выше 
        try:
            open("usersettings/" + str(str(message.from_id)) + ".txt", 'r')
        except:
            open("usersettings/" + str(message.from_id) + ".txt", 'w')

        with open("usersettings/" + str(message.from_id) + ".txt", "r+") as file:
            lines = file.readlines() 
            if len(lines) == 0:
                file.write("urlchannel: " + '' + "\n")
                file.write('speed: ' + message.text + "\n")
                file.write('limit: ' + "" + "\n")
                lines = file.readlines()
                file.close()
            else:
                lines[1] = "speed: " + message.text + "\n"
                with open("usersettings/" + str(message.from_id) + ".txt", "w") as newfile:
                    for line in lines:
                        newfile.write(line)        
                newfile.close()
        file.close()      

        waitanswer = False 
        waitUrlChannel = False
        waitSpeed = False
        waitLimit = False
        await message.reply(msg.speedGood, reply_markup=settings_kb)

    elif waitLimit == True: # Описано выше 
        try:
            open("usersettings/" + str(str(message.from_id)) + ".txt", 'r')
        except:
            open("usersettings/" + str(message.from_id) + ".txt", 'w')

        with open("usersettings/" + str(message.from_id) + ".txt", "r+") as file:
            lines = file.readlines() 
            if len(lines) == 0:
                file.write("urlchannel: " + "" + "\n")
                file.write('speed: ' + "" + "\n")
                file.write('limit: ' + message.text + "\n")
                lines = file.readlines()
                file.close()
            else:
                lines[2] = "limit: " + message.text + "\n"
                with open("usersettings/" + str(message.from_id) + ".txt", "w") as newfile:
                    for line in lines:
                        newfile.write(line)        
                newfile.close()
        file.close()      

        waitanswer = False
        waitUrlChannel = False
        waitSpeed = False
        waitLimit = False
        await message.reply(msg.urlGood, reply_markup=settings_kb)

    else:
        await message.reply('Я такого не понимаю :( Выберите команду и тогда я постораюсь лучше распознать текст :)')



if __name__ == '__main__':    
    waitanswer = False # Если мы хотим получить ответ от пользователя делаем True дабы исключить командные сообщения
    waitUrlChannel = False # Если ожидаем ответ для редактирования настроек
    waitSpeed = False
    waitLimit = False
    waitUrlYandex = False
    
    executor.start_polling(dp, skip_updates=True) # Стартуем!
